import React  from 'react';
import { Card, CardImg, CardImgOverlay, Breadcrumb,BreadcrumbItem,
  CardTitle } from 'reactstrap';
import {Link} from 'react-router-dom';
import { Loading } from './LoadingComponent';


  function RenderDish({dish}){
    return(

     <Link to={`/menu/${dish.id}`} >
       <Card>
        <CardImg width="100%"  src={dish.image} alt={dish.name} />
          <CardImgOverlay>
           <CardTitle>{dish.name}</CardTitle>
          </CardImgOverlay>
       </Card>
      </Link>
    );
  }


  // componentDidMount(){
  //   console.log("Menu compoenent didmount invoked,")
  // }

   const Menu = (props) => {
         const menu = props.dishes.dishes.map((dish) => {


        if (props.dishes.isLoading) {
            return(
                <div className="container">
                    <div className="row">            
                        <Loading />
                    </div>
                </div>
            );
        }
        else if (props.dishes.errMess) {
            return(
                <div className="container">
                    <div className="row"> 
                        <div className="col-12">
                            <h4>{props.dishes.errMess}</h4>
                        </div>
                    </div>
                </div>
            );
        }
        else
          return (
            <div  className="col-12  col-md-5 mt-5">
              <RenderDish dish={dish} />
            </div>
          );
      })

      return(
        <div className="container">
          <Breadcrumb>
              <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
              <BreadcrumbItem active>Menu</BreadcrumbItem>
          </Breadcrumb>
          <div className="col-12">
            <h3>Menu</h3>
              <hr/>
          </div>   
          <div className="row">
              {menu}
          </div>
        </div>
     );

   }
  
    
    // console.log("Menu compoenent render invoked,")
   


export default Menu;