/* eslint-disable react/jsx-pascal-case */
import React  from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Card,Button, CardImg, CardText, CardBody,
  CardTitle,Breadcrumb,BreadcrumbItem,Modal,ModalHeader,ModalBody,Form,FormGroup,Label,Input,Row,Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import { Component } from 'react';
import { Control, LocalForm, Errors } from 'react-redux-form';
import { Loading } from './LoadingComponent';
//import { addComment } from '../redux/ActionCreators';

 
   function RenderDishDetailCard({dishDetail}){
        return(
                <Card>
                    <CardImg top src={dishDetail.image} alt={dishDetail.name} />
                      <CardBody>
                        <CardTitle>{dishDetail.name}</CardTitle>
                        <CardText>{dishDetail.description}</CardText>
                      </CardBody>
                </Card>
        )
    }

   function RenderComment({comments,addComment,dishId}){
    // console.log("Dishdetail --"+dishDetail)
        if(comments !=null)
          return(
            <div className="col 12 col-md-5 m-1" >
                <h4>Comments</h4>
                <ul className="list-unstyled">
                {comments.map( comment => {
                return (
                <ul class = "list-unstyled"> 
                    <li>{comment.author}</li>
                    <li>{comment.comment}</li>
                    <li>{comment.date}</li>
                </ul>
                );
               })}
              </ul>
              <CommentForm dishId={dishId} addComment={addComment} />
              </div>
        );
        else
        return (

          <React.Fragment></React.Fragment>
        );
    }

   //  prop is coming in from Container component - MainComponent.js
    const DishDetail = (props) => {


       if (props.isLoading) {
            return(
                <div className="container">
                    <div className="row">            
                        <Loading />
                    </div>
                </div>
            );
        }
        else if (props.errMess) {
            return(
                <div className="container">
                    <div className="row">            
                        <h4>{props.errMess}</h4>
                    </div>
                </div>
            );
        }
    
       else if( props.dish != null){
        return(
          <div class="container">
            <div class="row">
                <Breadcrumb>
                  <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                  <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                </Breadcrumb> 
                <div className="col-12">
                  <h3>{props.dish.name}</h3>
                    <hr />
                </div> 
            </div>

            <div class="row">
              <div class="col-12 col-md-5 ml-1">
                   <RenderDishDetailCard dishDetail ={props.dish} />
                </div>
                <div class="col-12 col-md-5 ml-1">
                   
                    <RenderComment comments = {props.comments} 
                                   addComment = {props.addComment}
                                   dishId = {props.dish.id}
                    />  
                </div>
            </div>
          </div>
        );
       }else{ return (<div></div>);}
    }
  
    const maxLength = (len) => (val) => !(val) || (val.length <= len);
    const minLength = (len) => (val) => val && (val.length >= len);

    class CommentForm extends Component{
      constructor(props){
        super(props);
        this.state ={
          isModalOpen: false
          };
          this.toggleModal = this.toggleModal.bind(this);
          this.handleSubmit = this.handleSubmit.bind(this);
      }
      toggleModal() {
        this.setState({
          isModalOpen: !this.state.isModalOpen
        });
      }
      handleSubmit(values){
        // console.log('values ' + JSON.stringify(values));
        // alert('Current State is: ' + JSON.stringify(values));
        // alert('Values    ',values.author, values.rating, values.comment);
        // //event.preventDefault();
        this.toggleModal();
        this.props.addComment(this.props.dishId,values.rating, values.author, values.comment);
      }
      render(){
        return (
          <div>
          <Button onClick={this.toggleModal} className="mb-2">
            <i className="fa fa-pencil mr-2"></i>
            Submit Comment
          </Button>
          <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
              <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
              <ModalBody>
              <LocalForm onSubmit={(values)=>this.handleSubmit(values)}>
                            <Row className="form-group">
                            <Label htmlFor="message" className="ml-2" >Rating</Label>
                                <Col md={12}>
                                    <Control.select model=".rating" name="rating"
                                        className="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </Control.select>
                                </Col>
                            </Row>

                            <Row className="form-group">
                                <Label htmlFor="name" className="ml-2">Your Name</Label>
                                <Col md={12}>
                                    <Control.text model=".author" id="author" name="author"
                                        placeholder="author"
                                        className="form-control" 
                                        validators={{
                                           minLength: minLength(3), maxLength: maxLength(15)
                                      }}
                                    />
                                    <Errors
                                        className="text-danger"
                                        model=".name"
                                        show="touched"
                                        messages={{
                                
                                            minLength: 'Must be greater than 2 characters',
                                            maxLength: 'Must be 15 characters or less'
                                        }}
                                     />
                                </Col>
                            </Row>
                          
                            <Row className="form-group">
                                <Label htmlFor="message" className="ml-2" >Comment</Label>
                                <Col md={12}>
                                    <Control.textarea model=".comment" id="comment" name="comment"
                                        rows="6"
                                        className="form-control" />
                                </Col>
                            </Row>
                            <Row className="form-group">
                                <Col md={12}>
                                    <Button type="submit" color="primary">
                                    Submit
                                    </Button>
                                </Col>
                            </Row>
                        </LocalForm>
              </ModalBody>
           </Modal>
         </div>

           );
      }


    }

/* <p>{dishDetail.comments[0].author}</p> */
  export default DishDetail;